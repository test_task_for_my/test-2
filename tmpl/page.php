<?php

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
	<title>Тестовое задание 2</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<a href="/" class="btn btn-primary">Главная</a>
			<? if(empty($post)): ?>
				<p class="alert alert-info"> нет данных</p>
			<? else: ?>
				<p class="h2"><?=$post['name']?></p>
				<p class="text-justify">Здесь по идее должна распологаться информация о странице</p>
				<? Dump::xp($post, 1, '$post'); ?>
			<? endif ?>
		</div>
	</div>
</div>

<script type="text/javascript" src="/vendor/libra/jquery-assets/public/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
