<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
	<title>Тестовое задание 2</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-6">
			<p class="h3">не ЧПУ</p>
			<ul>
				<li><b>Страницы:</b>
					<ul>
						<li><a href="/index.php?page=pages&id=1">О компании</a></li>
						<li><a href="/index.php?page=pages&id=2">Новости</a></li>
						<li><a href="/index.php?page=pages&id=3">Условия сотрудничества</a></li>
						<li><a href="/index.php?page=pages&id=4">Контакты</a></li>
						<li><a href="/index.php?page=pages&id=5">Розничная торговля</a></li>
						<li><a href="/index.php?page=pages&id=6">Полезная информация</a></li>
					</ul>
				</li>
				<li><b>Пряжа для ручного вязания:</b>
					<ul>
						<li><a href="/index.php?page=catalog1&id=1">Италия</a></li>
						<li><a href="/index.php?page=catalog1&id=2">Перу</a></li>
						<li><a href="/index.php?page=catalog1&id=3">Австралия</a></li>
						<li><a href="/index.php?page=catalog1&id=4">ЮАР</a></li>
						<li><a href="/index.php?page=catalog1&id=5">Германия</a></li>
						<li><a href="/index.php?page=catalog1&id=6">Турция</a></li>
					</ul>
				</li>
				<li><b>Товары для рукоделия:</b>
					<ul>
						<li><a href="/index.php?page=catalog2&id=1">Спицы и крючки</a></li>
						<li><a href="/index.php?page=catalog2&id=2">Лупы</a></li>
						<li><a href="/index.php?page=catalog2&id=3">Шерсть для валяния</a></li>
						<li><a href="/index.php?page=catalog2&id=4">Вышивальные наборы &quot;Панорама&quot;</a></li>
						<li><a href="/index.php?page=catalog2&id=5">Наборы детские</a></li>
						<li><a href="/index.php?page=catalog2&id=6">Подушки</a></li>
						<li><a href="/index.php?page=catalog2&id=7">Подушки с рюшками детские</a></li>
						<li><a href="/index.php?page=catalog2&id=8">Наборы </a></li>
						<li><a href="/index.php?page=catalog2&id=9">Вышивальные наборы &quot;Орхидея&quot;</a></li>
						<li><a href="/index.php?page=catalog2&id=10">наборы</a></li>
						<li><a href="/index.php?page=catalog2&id=11">канва с рисунком</a></li>
						<li><a href="/index.php?page=catalog2&id=12">подушки</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="col-6">
			<p class="h3">ЧПУ</p>
			<ul>
				<li><b>Страницы:</b>
					<ul>
						<li><a href="/index.php?page=pages&id=1">О компании</a></li>
						<li><a href="/index.php?page=pages&id=2">Новости</a></li>
						<li><a href="/index.php?page=pages&id=3">Условия сотрудничества</a></li>
						<li><a href="/index.php?page=pages&id=4">Контакты</a></li>
						<li><a href="/index.php?page=pages&id=5">Розничная торговля</a></li>
						<li><a href="/index.php?page=pages&id=6">Полезная информация</a></li>
					</ul>
				</li>
				<li><b>Пряжа для ручного вязания:</b>
					<ul>
						<li><a href="/index.php?page=catalog1&id=1">Италия</a></li>
						<li><a href="/index.php?page=catalog1&id=2">Перу</a></li>
						<li><a href="/index.php?page=catalog1&id=3">Австралия</a></li>
						<li><a href="/index.php?page=catalog1&id=4">ЮАР</a></li>
						<li><a href="/index.php?page=catalog1&id=5">Германия</a></li>
						<li><a href="/index.php?page=catalog1&id=6">Турция</a></li>
					</ul>
				</li>
				<li><b>Товары для рукоделия:</b>
					<ul>
						<li><a href="/index.php?page=catalog2&id=1">Спицы и крючки</a></li>
						<li><a href="/index.php?page=catalog2&id=2">Лупы</a></li>
						<li><a href="/index.php?page=catalog2&id=3">Шерсть для валяния</a></li>
						<li><a href="/index.php?page=catalog2&id=4">Вышивальные наборы &quot;Панорама&quot;</a></li>
						<li><a href="/index.php?page=catalog2&id=5">Наборы детские</a></li>
						<li><a href="/index.php?page=catalog2&id=6">Подушки</a></li>
						<li><a href="/index.php?page=catalog2&id=7">Подушки с рюшками детские</a></li>
						<li><a href="/index.php?page=catalog2&id=8">Наборы </a></li>
						<li><a href="/index.php?page=catalog2&id=9">Вышивальные наборы &quot;Орхидея&quot;</a></li>
						<li><a href="/index.php?page=catalog2&id=10">наборы</a></li>
						<li><a href="/index.php?page=catalog2&id=11">канва с рисунком</a></li>
						<li><a href="/index.php?page=catalog2&id=12">подушки</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
</body>
</html>