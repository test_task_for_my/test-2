<?php
require __DIR__ . '/config.php';
require __DIR__ . '/vendor/autoload.php';

$post = Route::getInstance()->getPost();

if(empty($post)){
	require __DIR__ . '/tmpl/map.php';
}else{
	require __DIR__ . '/tmpl/page.php';
}
