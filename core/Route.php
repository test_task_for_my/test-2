<?php

/**
 * @property $route  - алиас
 * @property $postId - ид записи (продукт/категория/статья и т.д.)
 * @property $page   - например шаблон/способ отображения записи
 */
class Route{
	private $route = '';
	private $postId = 0;
	private $page = '';
	// --------------------------------------------------------
	// --[ BEGIN Singleton ]----------------------------
	private static $_instance;


	private function __construct(){
		$this->route = trim(substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')), '/');
		$this->route  = trim($this->route ,'index.php');
		//Dump::xd($this->route, 1, '$this->route');
		if(empty($this->route)){
			// пустой чпу запрос пытаемся определить раратетры из технического урл
			$this->page = $_GET['page'];
			$this->postId = (int) $_GET['id'];
		}else{
			$res = DBClient::getInstance()->select('routs', 'alias=?', [$this->route]);
			if( ! empty($res)){
				$this->page = $res[0]['page'];
				$this->postId = $res[0]['id'];
			}
		}
	}

	public static function getInstance(){
		if(is_null(self::$_instance)) self::$_instance = new self();
		return self::$_instance;
	}

	// --[ END Singleton ]----------------------------
	// --------------------------------------------------------


	/**
	 * Вернёт данные о странице или выбросит 404 ошибку
	 */
	public function getPost(){
		if(empty($this->page) or empty($this->postId)) exit(require __DIR__ . '/../tmpl/map.php');
		$post = DBClient::getInstance()->select('routs', ' post_id=? and page=? ', [$this->postId, $this->page]);
		if(empty($post)) exit(require __DIR__ . '/../tmpl/404.php');
		return $post[0];
	}
}
