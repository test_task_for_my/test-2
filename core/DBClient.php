<?php

class DBClient{
	public $pdo;
	private $time;
	private $listTable = array();
	private $dateFormat = 'Y-m-d_H-i-s';
	private $pdoOptions = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	                            PDO::ATTR_PERSISTENT => false,);

	// --------------------------------------------------------
	// --[ BEGIN Singleton ]----------------------------
	private static $_instance;

	private function __construct(){
		$this->time = time();

		try{
			$this->pdo = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, $this->pdoOptions);
			$this->pdo->query('SET NAMES utf8');
		}catch(PDOException $e){
			//Log::getInstance()->w($this->excToStr($e));Dump::xd($this->excToStr($e));
			echo $this->excToStr($e);
		}
	}

	protected function __clone(){ }

	public static function getInstance(){
		if(is_null(self::$_instance)) self::$_instance = new self();
		return self::$_instance;
	}

	// --[ END Singleton ]----------------------------
	// --------------------------------------------------------

	public function lastInsertId(){ return $this->pdo->lastInsertId(); }

	/*
	 * преобразует ошибку в строку
	 * */
	private function excToStr(PDOException $e){
		return 'ERROR [' . $e->getCode() . '] File: ' . $e->getFile() . ' Line: ' . $e->getLine() . ' (' . $e->getMessage() . ')';
	}

	/*
	 * получаем записи из таблицы (можно отправить любой запрос к бд на выборку)
	 * */
	public function select($from = '', $sqlWhere = '', $parWhere = array(), $select = '*', $otherSQL = ''){
		if( ! is_string($from) or empty($from)) return false;
		try{
			$sql = "SELECT $select FROM $from ";

			if(is_string($sqlWhere) and ! empty($sqlWhere)) $sql .= " WHERE $sqlWhere";
			$sql .= "  $otherSQL ";

			$prepare = $this->pdo->prepare($sql);
			if( ! empty($parWhere) and is_array($parWhere)) $prepare->execute($parWhere);else
				$prepare->execute();
			return $prepare->fetchAll();
		}catch(PDOException $e){
			//Log::getInstance()->w($this->excToStr($e));Dump::xd($this->excToStr($e));
			echo $this->excToStr($e);
		}
		return false;
	}

	/*
	 * добавляет одну запись в таблицу
	 * $table - имя таблицы
	 * $param - массив значкений полей ['название колонки',  значение]
	 * */
	public function insert($table = '', $param = array()){
		if(empty($table) or ! is_string($table)) return false;
		if(empty($param) or ! is_array($param)) return false;
		try{
			$sql = "INSERT INTO $table (";
			$sqlV = '';
			foreach(array_keys($param) as $item){
				$sql .= " `$item`, ";
				$sqlV .= " ?,";
			}
			$sqlV = trim($sqlV, ',');
			$sql = trim($sql, ', ');
			$sql .= ") VALUES ($sqlV);";
			if($this->pdo->prepare($sql)->execute(array_values($param))) return $this->pdo->lastInsertId();else false;
		}catch(PDOException $e){
			//Log::getInstance()->w($this->excToStr($e));Dump::xd($this->excToStr($e));
			echo $this->excToStr($e);
		}
		return false;
	}

	/*
	 * обновит значения записей в таблице
	 * $table - имя таблицы
	 * $parUp - массив обновляемых значкений полей ['название колонки',  значение]
	 * $sqlW - sql условие вида id=? OR name=?
	 * $parW - массив параметров для условия
	 * */
	public function update($table = '', $parUp = array(), $sqlWhere = '', $parWhere = array()){
		if(empty($table) or ! is_string($table)) return false;
		if(empty($sqlWhere) or ! is_string($sqlWhere)) return false;
		if(empty($parUp) or ! is_array($parUp)) return false;
		//if(empty($parWhere) or ! is_array($parWhere)) return false;
		if( ! is_array($parWhere)) return false;
		try{
			$sql = "UPDATE $table SET ";
			foreach(array_keys($parUp) as $item) $sql .= " `$item`=?, ";
			$sql = trim($sql, ', ');
			$sql .= ' WHERE ' . $sqlWhere . ' ;';
			return $this->pdo->prepare($sql)->execute(array_merge(array_values($parUp), $parWhere));
		}catch(PDOException $e){
			//Log::getInstance()->w($this->excToStr($e));Dump::xd($this->excToStr($e));
			echo $this->excToStr($e);
		}
		return false;
	}

	/*
	 * обновит значения записей в таблице
	 * $table - имя таблицы
	 * $sqlW - sql условие вида id=? OR name=?
	 * $parW - массив параметров для условия
	 * */
	public function delete($table = '', $sqlWhere = '', $parWhere = array()){
		if(empty($table) or ! is_string($table)) return false;
		if(empty($sqlWhere) or ! is_string($sqlWhere)) return false;
		try{
			$sql = "DELETE FROM $table WHERE $sqlWhere ;";
			if(empty($parWhere) or ! is_array($parWhere)) return $this->pdo->prepare($sql)->execute();else
				return $this->pdo->prepare($sql)->execute($parWhere);
		}catch(PDOException $e){
			//Log::getInstance()->w($this->excToStr($e));Dump::xd($this->excToStr($e));
			echo $this->excToStr($e);
		}
		return false;
	}

	/*
	 * вернёт список таблиц и их описание
	 * */
	public function getListTable(){
		if( ! empty($this->listTable)) return $this->listTable;
		return $this->listTable = $this->pdo->query('SHOW TABLE STATUS FROM ' . DB_NAME)->fetchAll();
	}

	/*
	 * вернёт структуру таблицы
	 * */
	public function getStructureTable($table){
		return $this->pdo->query('SHOW CREATE TABLE ' . $table)->fetch();
	}

	/*
	 *  вернёт структуру всех таблиц
	 * */
	public function getStructureAllTables(){
		$lT = $this->getListTable();
		$res = array();
		if( ! empty($lT)) foreach($lT as $it) $res[] = $this->getStructureTable($it['Name']);
		return $res;
	}

	/*
	 * вернёт данные для навигации по таблице
	 * */
	public function getPageNav($table, $pageSize = 10, $idCol = '*'){
		$sql1 = "SELECT count($idCol) as `count`  FROM $table";
		$count = $this->pdo->query($sql1)->fetch();
		$count = $count['count'];
		$countPage = ceil($count / $pageSize);
		return array('C' => $count, 'CP' => $countPage, 'PS' => $pageSize,);
	}

	/*
	 * вернёт таблицу по её номеру
	 * */
	public function getPage($table, $page = 0, $pageSize = 10, $select = '*'){
		$start = $page * $pageSize;
		return $this->select($table, '', array(), $select, "LIMIT $start, $pageSize");
	}

	/*
	 * сбросит кеш таблицы
	 * */
	public function flushT($table){
		$this->pdo->exec("FLUSH TABLE $table");
	}

	/*
	 * сохранит или вернёт структуру всех таблиц
	 * */
	public function saveStructureAllTables($dir = null, $save = true){
		if( ! $dir) $dir = __DIR__;
		$dir = rtrim($dir, '/');
		@mkdir($dir);
		$structureAllTables = $this->getStructureAllTables();
		$sql = '';
		if( ! empty($structureAllTables) and is_array($structureAllTables)) foreach($structureAllTables as &$structureTable){
			$sql .= "/* === === === === === */\n";
			$sql .= "/*" . $structureTable['Table'] . "*/\n";
			$sql .= $structureTable['Create Table'] . ";\n\n";
		}
		$fn = $dir . '/damp-structure-' . DB_NAME . '-' . date($this->dateFormat, $this->time) . '.sql';
		if($save){
			file_put_contents($fn, $sql);
			return $fn;
		}else{
			return $sql;
		}
	}

	/*
	 * сохраняет или возвращет дамп данных всех таблиц
	 * */
	public function saveDataAllTables($dir = null, $save = true){
		if( ! $dir) $dir = __DIR__;
		$dir = rtrim($dir, '/');
		@mkdir($dir);
		$lTables = $this->getListTable();
		$sql = '';
		foreach($lTables as $table) $sql .= $this->saveDataTable($table['Name']);
		$fn = $dir . '/dump-data-' . DB_NAME . '-allTable-' . date($this->dateFormat, $this->time) . '.sql';
		if($save){
			file_put_contents($fn, $sql);
			return $fn;
		}else{
			return $sql;
		}
	}

	/*
	 * вернёт TRUE если таблица существует
	 * */
	public function tableExists($table = ''){
		if(empty($table) or ! is_string($table)) return false;
		$lT = $this->getListTable();
		if(empty($lT)) return false;
		foreach($lT as &$iT) if($iT['Name'] === $table) return true;
		return false;
	}

	/*
	 * сохранит / вернёт дамп данных одной таблицы
	 * */
	public function saveDataTable($table, $save = false, $dir = null){
		if(empty($table) or ! is_string($table)) return '';
		if( ! $dir) $dir = __DIR__;
		$dir = rtrim($dir, '/');
		@mkdir($dir);
		$sql = '';
		if( ! $this->tableExists($table)) return $sql;
		$pageNav = $this->getPageNav($table);
		if($pageNav['C'] == 0) return $sql;
		for($page = 0; $page < $pageNav['CP']; $page ++){
			$pageData = $this->getPage($table, $page);
			if(empty($pageData)) continue;
			$sql .= "/* === === === === === */\n";
			$sql .= "/* === " . $table . " === */\n";
			$sql .= "INSERT INTO `" . $table . "` VALUES \n";
			foreach($pageData as &$values){
				$sql .= '(';
				foreach($values as &$value){
					$sql .= "'" . str_replace("'", "''", $value) . "',";
				}
				$sql = trim($sql, ',');
				$sql .= '),';
			}
			$sql = trim($sql, ',');
			$sql .= ";\n\n";
		}
		$fn = $dir . '/dump-data-' . DB_NAME . '-' . $table . '-' . date($this->dateFormat, $this->time) . '.sql';
		if($save){
			file_put_contents($fn, $sql);
			return $fn;
		}else{
			return $sql;
		}
	}
}