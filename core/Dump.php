<?php

class Dump{
	public static function ip($ip = array('31.129.6.27', '176.123.194.187',)){
		if(is_string($ip)) if($_SERVER['REMOTE_ADDR'] == $ip) return true;else return false;
		if(is_array($ip)) return in_array($_SERVER['REMOTE_ADDR'], $ip);
		return false;
	}

	public static function xPrint($var = null, $isP = true, $style = '', $title = null){
		if($isP){
			ob_start();
			print_r($var);
			$info = ob_get_clean();
		}else{
			ob_start();
			var_dump($var);
			$info = ob_get_clean();
		}
		if(self::isTerminal()){
			echo '----------[ ' . @$title[0] . ' ' . @$title[1] . ' ]----------' . PHP_EOL . $info . PHP_EOL . '----------------------------------------' . PHP_EOL;
		}else{
			echo $style, '<div class="xPrintWrapper"><div class="xPrintTitle">' . '<span>' . $title[0] . '</span> ' . $title[1] . '</div><pre>' . $info . '</pre></div>';
		}
	}

	private static function prepare($title = null){
		ini_set('xdebug.var_display_max_depth', 50);
		ini_set('xdebug.var_display_max_children', 25600);
		ini_set('xdebug.var_display_max_data', 999999999);
		$debug_backtrace = debug_backtrace();
		if(isset($debug_backtrace[1])) $debug_backtrace = $debug_backtrace[1];

		if( ! empty($title)){
			$sTitle[0] = "$title -> ";
			if( ! $_SERVER['DOCUMENT_ROOT']) @$sTitle[1] .= $debug_backtrace['file'];else{
				$sTitle[1] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $debug_backtrace['file']);
				if(strlen($sTitle[1]) == strlen($debug_backtrace['file'])){
					$temp = str_replace('/', '\\', $_SERVER['DOCUMENT_ROOT']);
					$sTitle[1] = str_replace($temp, '', $debug_backtrace['file']);
				}
			}
			$sTitle[1] .= ' :: ' . $debug_backtrace['line'];
		}else{
			if( ! $_SERVER['DOCUMENT_ROOT']) $sTitle[1] = $debug_backtrace['file'];else{
				$sTitle[1] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $debug_backtrace['file']);
				if(strlen($sTitle[1]) == strlen($debug_backtrace['file'])){
					$temp = str_replace('/', '\\', $_SERVER['DOCUMENT_ROOT']);
					$sTitle[1] = str_replace($temp, '', $debug_backtrace['file']);
				}
			}
			$sTitle[1] .= ' :: ' . $debug_backtrace['line'];
		}
		$style = '<style>.xPrintWrapper{color: #000;background: #f6f6f6;font-size: 14px;font-family: DialogInput , Monospaced, sans-serif;padding: 10px;margin: 0 15px 0 0;position: relative;top: 15px;border: 1px solid gray;width: 80%;z-index: 1000;}.xPrintWrapper .xPrintTitle{font-weight: bold;padding: 4px 0 4px 5px;color: #000;background: #dddddd;top: -15px;min-height: 15px;border: 1px solid gray;z-index: 1000;font-family: DialogInput , Monospaced, sans-serif;overflow: hidden;line-height: inherit;height: auto;}.xPrintWrapper .xPrintTitle span{color: #fff;background: red;padding: 2px;margin: 0 3px 0 0;}.xPrintWrapper pre{z-index: 1000;color: black;font-size: 14px;line-height: 1;}</style>';
		return array('t' => $sTitle, 's' => $style,);
	}

	public static function xp($var = null, $isP = true, $title = null){
		$prepare = self::prepare($title);
		self::xPrint($var, $isP, $prepare['s'], $prepare['t']);
	}

	public static function xd($var = null, $isP = true, $title = null){
		$prepare = self::prepare($title);
		self::xPrint($var, $isP, $prepare['s'], $prepare['t']);
		if(self::isTerminal()) exit(__FILE__ . '::' . __LINE__ . PHP_EOL);else
			exit('<p>' . __FILE__ . '::' . __LINE__ . '</p>');
	}

	public static function isTerminal(){ return PHP_SAPI === 'cli'; }

	public static function xPrintFile($var = null, $isP = true, $title = null){
		$prepare = self::prepare($title);
		$string = date(DATE_ATOM) . ' - ' . implode(' :: ', $prepare['t']) . PHP_EOL;
		if($isP){
			ob_start();
			print_r($var);
			$string .= ob_get_clean();
		}else{
			ob_start();
			var_dump($var);
			$string .= ob_get_clean();
		}
		$string .= '======================' . PHP_EOL . PHP_EOL;
		$n = '/' . __METHOD__ . '.txt';
		file_put_contents(__DIR__ . $n, $string, FILE_APPEND);
		echo "<p><a href='$n' target='_blank'>$n</a></p>";
	}
}