DROP TABLE IF EXISTS routs;

CREATE TABLE IF NOT EXISTS routs (
	id        INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	`name`    VARCHAR(255)                   NOT NULL,
	`alias`   VARCHAR(255)                   NOT NULL UNIQUE,
	`page`    VARCHAR(255)                   NOT NULL,
	`post_id` INTEGER                            NOT NULL
);

INSERT INTO routs (name, alias, page, post_id) VALUES
	('О компании', 'o-kompanii-1', 'pages', 1),
	('Новости', 'novosti-2', 'pages', 2),
	('Условия сотрудничества', 'usloviia-sotrudnichestva-3', 'pages', 3),
	('Контакты', 'kontakty-4', 'pages', 4),
	('Розничная торговля', 'roznichnaia-torgovlia-5', 'pages', 5),
	('Полезная информация', 'poleznaia-informatciia-6', 'pages', 6),
	('Италия', 'italiia-1', 'catalog1', 1),
	('Перу', 'peru-2', 'catalog1', 2),
	('Австралия', 'avstraliia-3', 'catalog1', 3),
	('ЮАР', 'iuar-4', 'catalog1', 4),
	('Германия', 'germaniia-5', 'catalog1', 5),
	('Турция', 'turtciia-6', 'catalog1', 6),
	('Спицы и крючки', 'spitcy-i-kriuchki-1', 'catalog2', 1),
	('Лупы', 'lupy-2', 'catalog2', 2),
	('Шерсть для валяния', 'sherst-dlia-valianiia-3', 'catalog2', 3),
	('Вышивальные наборы Панорама', 'vyshivalnye-nabory-panorama-4', 'catalog2', 4),
	('Наборы детские', 'nabory-detskie-5', 'catalog2', 5),
	('Подушки', 'podushki-6', 'catalog2', 6),
	('Подушки с рюшками детские', 'podushki-s-riushkami-detskie-7', 'catalog2', 7),
	('Наборы', 'nabory-8', 'catalog2', 8),
	('Вышивальные наборы Орхидея', 'vyshivalnye-nabory-orhideia-9', 'catalog2', 9),
	('наборы', 'nabory-10', 'catalog2', 10),
	('канва с рисунком', 'kanva-s-risunkom-11', 'catalog2', 11),
	('подушки', 'podushki-12', 'catalog2', 12);